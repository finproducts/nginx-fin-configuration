# Setting up NGINX with FIN 5

For production systems [J2 Innovations](https://www.j2inn.com/) recommends using [NGINX](https://www.nginx.com/) as a secure proxy to the FIN framework.

Therefore all HTTPS traffic is directed to NGINX. NGINX then proxies the traffic to a running instance of the FIN framework.

This repo contains the common configuration files required to use NGINX with FIN.

## FIN Configuration

Once you have FIN installed, go to the installation directory and edit `{finInstallDir}/etc/web/config.props`. Ensure `secureSessionCookie=true`. This will ensure the session cookies are marked as secure.

## NGINX Configuration

The configuration files listed provide HTTPS support (no HTTP). Please note, a valid certificate will still need to be installed alongside these configuration files.

Please see the [NGINX web site](https://www.nginx.com/) for details on how to configure NGINX.

We always recommend customers use the latest stable build of NGINX with the latest security patches!
